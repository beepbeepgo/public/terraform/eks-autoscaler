output "cluster_autoscaler_policy" {
  description = "Name of iam policy for the eks cluster autoscaler"
  value       = module.eks-autoscaler.cluster_autoscaler_policy
}

output "cluster_autoscaler_role_name" {
  description = "Name of the iam role for the eks cluster autoscaler"
  value       = module.eks-autoscaler.cluster_autoscaler_role_name
}
