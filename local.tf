locals {
  oidc_issuer = replace(var.eks_cluster_identity_oidc_issuer, "https://", "")
}
