# 1.0.0 (2022-12-31)


### Features

* initial release ([2b61063](https://gitlab.com/beepbeepgo/public/terraform/eks-autoscaler/commit/2b61063c436c911ea21d759e9041cc4563173937))

# 1.0.0-issue-init.1 (2022-12-31)


### Features

* init release ([266ca9f](https://gitlab.com/beepbeepgo/public/terraform/eks-autoscaler/commit/266ca9f72889f56355281849a4600f63ec377d1d))
